<?php

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Spatie\YamlFrontMatter\YamlFrontMatter;
Route::get('/', function () {            
    return view('home');
});
Route::get('/posts', function () {            
    return view('posts', [
        'posts' => Post::latest()->get()
        // 'posts' => Post::latest()->with('category','author')->get()
    ]);
});
Route::get('post/{post:slug}', function (Post $post) {
    return view('post', [
        'post' => $post
    ]);

    // or 
    // $post = cache()->remember('post/{post}',10, function() use ($path){
    //     echo "cached called";
    //     return file_get_contents($path);
    // });
});

// constraint for URL
// ->where('post', '[A-z_\-0-9]+')


// Route::get('post/{post}', function($slug){
//     return view('post',[
//         'post' => file_get_contents(__DIR__ . "/../resources/posts/{$slug}.html")
//     ]);
// })->whereAlphaNumeric('post');

Route::get('/categories', function(){
    return view('category',[
        'cat' => Category::all(),
    ]);
});

Route::get('/categories/{category:slug}', function(Category $category){
    return view('posts', [
        'posts' => $category->posts
        // 'posts' => $category->posts->load(['category', 'author'])
    ]);
});
Route::get('/author/{author:username}', function(User $author){
    return view('posts', [
        'posts' => $author->posts
        // 'posts' => $author->posts->load(['category', 'author'])
    ]);
});
Route::get('/authors', function(){
    return view('authors',[
        'cat' => User::all(),
    ]);
});
